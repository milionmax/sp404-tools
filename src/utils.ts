export function bytesToInt(bytes: Buffer, be = true): number {
  let hexes: string[] = []
  for (let base10 of bytes) {
    hexes.push(base10.toString(16).padStart(2, '0'))
  }

  if (!be) { // little endian, need to reverse
    hexes = hexes.reverse()
  }
  return parseInt(hexes.join(''), 16)
}

export function intToHex(num: number, bytes: number, le = false): string {
  // Convert to hex, pad start because we need little endian
  const beHex = num.toString(16).padStart(bytes * 2, '0').toUpperCase()
  if (!le) return beHex

  // split into two-char pairs, reverse, and join
  let pairs = beHex.match(/.{1,2}/g) || []

  return pairs.reverse().join('')
}
