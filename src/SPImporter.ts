import fs from 'fs'

import Drive from './Drive'
import DriveStruct from './interfaces/DriveStruct'
import PadInfo from './PadInfo'
import SampleStruct from './interfaces/SampleStruct'

export default class SPImporter {
  public drive: Drive

  constructor(drive: DriveStruct) {
    this.drive = Drive.fromStruct(drive)
  }

  async importSamples(): Promise<SampleStruct[]> {
    // Read PAD_INFO.BIN
    const padInfoPath = this.drive.padInfoPath
    const padInfoBuff = await fs.promises.readFile(padInfoPath)

    // Create Samples
    let samples: SampleStruct[] = []
    for (let offset = 0; offset < PadInfo.NUM_BYTES; offset += 32) {
      const lineNo = Math.floor(offset / 32)
      const [ bankIndex, padIndex ] = PadInfo.indexFromLineNo(lineNo)
      const padInfo = PadInfo.fromBuffer(padInfoBuff.slice(offset, offset+32))
      const samplePath = padInfo.isEmptyPad()
        ? '' 
        : this.drive.samplePathFromIndex(bankIndex, padIndex)

      samples.push({
        bankIndex,
        padIndex,
        sourcePath: samplePath,
        origPadInfo: padInfo.toStruct()
      })
    }

    return samples
  }
}
