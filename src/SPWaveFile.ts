import fs from 'fs'
import get from 'lodash/get'
import { WaveFile } from 'wavefile'

import { intToHex } from './utils'

/*** header reference: https://gist.github.com/threedaymonk/701ca30e5d363caa288986ad972ab3e0 ***/

const TARGET_SAMPLE_RATE = 44100
const TARGET_BIT_DEPTH = 16
const UNKNOWN_SAMPLE_RATE = -1
const UNKNOWN_BIT_DEPTH = -1

const RIFF = '52494646' // RIFF
const WAVE = '57415645' // WAVE
const FMT =  '666d7420' // fmt_
const RLND = '524c4e44' // RLND
const DATA = '64617461' // data

export default class SPWaveFile {
  private constructor(private wav: WaveFile) {}

  static fromBuffer(buff: Buffer): SPWaveFile {
    let wav = new WaveFile()
    wav.fromBuffer(buff)
    let spWav = new SPWaveFile(wav)
    spWav.ensureSPFidelity()
    return spWav
  }

  public get sampleRate(): number {
    return get(this.wav.fmt, 'sampleRate', UNKNOWN_SAMPLE_RATE)
  }

  public get bitDepth(): number {
    return get(this.wav.fmt, 'bitsPerSample', UNKNOWN_BIT_DEPTH)
  }

  private get samples(): Uint8Array {
    return get(this.wav.data, 'samples', new Uint8Array(0))
  }

  public get sampleLength(): number {
    return this.samples.length
  }

  public get numChannels(): number {
    return get(this.wav.fmt, 'numChannels', 1)
  }

  private get blockAlign(): number {
    return get(this.wav.fmt, 'blockAlign', 2)
  }

  /**
   * Duration of sample in seconds
   * Note: SP-404SX will not load samples with duration <100ms
   */
  public get duration(): number {
    return this.wav.getSamples(true).length / this.sampleRate / this.numChannels
  }

  private ensureSPFidelity(): void {
    // Convert to SP-404SX native fidelity
    if (this.sampleRate !== TARGET_SAMPLE_RATE) {
      this.wav.toSampleRate(TARGET_SAMPLE_RATE)
    }
    if (this.bitDepth !== TARGET_BIT_DEPTH) {
      this.wav.toBitDepth(TARGET_BIT_DEPTH.toString())
    }
  }

  private getRIFFChunkHex() {
    // data always starts at 512th byte
    // also minus 8 b/c we don't include these 4 or "RIFF"
    const chunkSize = 504 + this.samples.length 
    const chunkSizeHex = intToHex(chunkSize, 4, true)
    return `${RIFF}${chunkSizeHex}${WAVE}`
  }

  private getFMTChunkHex() {
    return '' +
      FMT +
      '12000000' + // ChunkSize: 18 bytes always
      '0100' + // AudioFormat: PCM
      intToHex(this.numChannels, 2, true) + // mono or stereo
      '44AC0000' + // SampleRate: 44100Hz always
      intToHex(this.blockAlign * this.sampleRate, 4, true) + // ByteRate: see ref
      intToHex(this.blockAlign, 2, true) + // BlockAlign: see ref
      '1000' + // BitsPerSample: 16-bit
      '0000' // ExtraParamSize ??
  }

  private getRLNDChunkHex(padIndex: number) {
    // Pad 445 bytes worth of 0's for correct chunk size
    const padding = ''.padEnd(445 * 2, '0') 

    return '' +
      RLND + 
      'CA010000' + // ChunkSize: always this
      '726f696673707378' + // Device: roifspsx
      '04000000' + // Unknown
      intToHex(padIndex, 1, true) + // SampleIndex: A1=00h, J12=77h
      padding
  }

  private getDATAChunkHex() {
    // not including actual samples here, write those directly
    const chunkSize = get(this.wav.data, 'chunkSize', 0)
    return DATA + intToHex(chunkSize, 4, true)
  }

  /**
   * padIndex: absolute index from 0 - 119
   */
  async write(padIndex: number, destPath: string): Promise<any> {
    const headerStr = this.getRIFFChunkHex() +
      this.getFMTChunkHex() +
      this.getRLNDChunkHex(padIndex) +
      this.getDATAChunkHex()

    const header = Buffer.from(headerStr, 'hex')
    const samples = Buffer.from(this.samples)
    const file = Buffer.concat([ header, samples ])
    return fs.promises.writeFile(destPath, file)
  }
}
