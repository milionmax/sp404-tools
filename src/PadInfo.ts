import PadInfoStruct from './interfaces/PadInfoStruct'
import PadInfoFields, { DEFAULTS } from './PadInfoFields'
import { bytesToInt, intToHex } from './utils'

const ORIG_SAMPLE_START_IDX = [0, 4]
const ORIG_SAMPLE_END_IDX   = [4, 8]
const USER_SAMPLE_START_IDX = [8, 12]
const USER_SAMPLE_END_IDX   = [12, 16]
const VOLUME_IDX            = 16
const LOFI_IDX              = 17
const LOOP_IDX              = 18
const GATE_IDX              = 19
const REVERSE_IDX           = 20
const FORMAT_IDX            = 21
const CHANNELS_IDX          = 22
const TEMPO_MODE_IDX        = 23
const ORIG_TEMPO_IDX        = [24, 28]
const USER_TEMPO_IDX        = [28, 32]

/***
 * Represents data stored in ONE LINE (32 bytes) 
 * of an SP-404SX's PAD_INFO.BIN file. One line
 * corresponds to a single pad on the device.
 */
export default class PadInfo {
  static NUM_BYTES = 120*32

  public fields: PadInfoFields

  constructor(fields = {}) {
    this.fields = Object.assign({}, DEFAULTS, fields)
  }

  static fromBuffer(buff: Buffer): PadInfo {
    return new PadInfo({
      origSampleStart: bytesToInt(buff.slice(...ORIG_SAMPLE_START_IDX)),
      origSampleEnd  : bytesToInt(buff.slice(...ORIG_SAMPLE_END_IDX)),
      userSampleStart: bytesToInt(buff.slice(...USER_SAMPLE_START_IDX)),
      userSampleEnd  : bytesToInt(buff.slice(...USER_SAMPLE_END_IDX)),
      volume         : buff[VOLUME_IDX],
      lofi           : !!buff[LOFI_IDX],
      loop           : !!buff[LOOP_IDX],
      gate           : !!buff[GATE_IDX],
      reverse        : !!buff[REVERSE_IDX],
      format         : buff[FORMAT_IDX],
      channels       : buff[CHANNELS_IDX],
      tempoMode      : buff[TEMPO_MODE_IDX],
      origTempo      : bytesToInt(buff.slice(...ORIG_TEMPO_IDX)),
      userTempo      : bytesToInt(buff.slice(...USER_TEMPO_IDX))
    })
  }

  toBuffer(): Buffer {
    return Buffer.concat([
      Buffer.from(intToHex(this.fields.origSampleStart, 4), 'hex'),
      Buffer.from(intToHex(this.fields.origSampleEnd, 4), 'hex'),
      Buffer.from(intToHex(this.fields.userSampleStart, 4), 'hex'),
      Buffer.from(intToHex(this.fields.userSampleEnd, 4), 'hex'),
      Uint8Array.from([this.fields.volume]),
      Uint8Array.from([this.fields.lofi ? 1 : 0]),
      Uint8Array.from([this.fields.loop ? 1 : 0]),
      Uint8Array.from([this.fields.gate ? 1 : 0]),
      Uint8Array.from([this.fields.reverse ? 1 : 0]),
      Uint8Array.from([this.fields.format]),
      Uint8Array.from([this.fields.channels]),
      Uint8Array.from([this.fields.tempoMode]),
      Buffer.from(intToHex(this.fields.origTempo, 4), 'hex'),
      Buffer.from(intToHex(this.fields.userTempo, 4), 'hex')
    ])
  }

  static indexFromLineNo(lineNo: number): [string, number] {
    const bankNo = Math.floor(lineNo / 12) // 0 - 10

    const bankIndex = String.fromCharCode(bankNo + 65) // A - J
    const padIndex = lineNo % 12 + 1 // 1 - 12
    return [ bankIndex, padIndex ]
  }

  isEmptyPad() {
    // No sample duration
    return this.fields.origSampleEnd === DEFAULTS.origSampleEnd
  }

  static fromStruct(struct: PadInfoStruct): PadInfo {
    return new PadInfo(struct.fields)
  }

  toStruct(): PadInfoStruct {
    return {
      fields: this.fields
    } as PadInfoStruct
  }
}
