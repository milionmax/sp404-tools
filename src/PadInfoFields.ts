export enum Format {
  AIFF, WAVE
}

export enum ChannelType {
  MONO = 1, STEREO
}

export enum TempoMode {
  OFF, PATTERN, USER
}

export const DEFAULTS: PadInfoFields = {
  // Sample data starts at byte offset 512
  // sample start and end markers are byte offsets in the file
  // a 512 start indicates the absolute beginning of the sample data
  // a 512 end indicates an empty pad (sample has no duration)
  origSampleStart : 512,
  origSampleEnd   : 512,
  userSampleStart : 512,
  userSampleEnd   : 512,
  volume          : 127,
  lofi            : false,
  loop            : false,
  gate            : true,
  reverse         : false,
  format          : Format.WAVE,
  channels        : ChannelType.STEREO,
  tempoMode       : TempoMode.OFF,
  origTempo       : 1200,
  userTempo       : 1200
}

export default interface PadInfoFields {
    origSampleStart: number,
    origSampleEnd: number,
    userSampleStart: number,
    userSampleEnd: number,
    volume: number,
    lofi: boolean,
    loop: boolean,
    gate: boolean,
    reverse: boolean,
    format: Format,
    channels: ChannelType,
    tempoMode: TempoMode,
    origTempo: number,
    userTempo: number
}
