export class BankIndexValidator {
  static isValid(index: string): boolean {
    return /^[A-J]$/.test(index)
  }
}

export class PadIndexValidator {
  static isValid(index: number): boolean {
    return Number.isInteger(index) && index >= 1 && index <= 12
  }
}
