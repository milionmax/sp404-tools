import path from 'path'

import DriveStruct from './interfaces/DriveStruct'
import { BankIndexValidator, PadIndexValidator } from './Validation'

const SMPL_DIR = path.join('ROLAND', 'SP-404SX', 'SMPL')
const PAD_INFO_PATH = path.join(SMPL_DIR, 'PAD_INFO.BIN')

export default class Drive {
  constructor(public path: string = '') {}

  get padInfoPath() {
    return path.join(this.path, PAD_INFO_PATH)
  }

  samplePathFromIndex(bankIndex: string, padIndex: number) {
    if (!BankIndexValidator.isValid(bankIndex)) {
      throw new Error(`Invalid bank identifier: ${bankIndex}`)
    }
    if (!PadIndexValidator.isValid(padIndex)) {
      throw new Error(`Invalid pad identifier: ${padIndex}`)
    }

    // Ex file names: A0000001.WAV, D0000011.WAV
    const zeros = ''.padEnd((padIndex < 10 ? 6 : 5), '0')
    const fileName = `${bankIndex}${zeros}${padIndex}.WAV`
    return path.join(this.path, SMPL_DIR, fileName)
  }

  static fromStruct(struct: DriveStruct): Drive {
    return new Drive(struct.path)
  }

  toStruct(): DriveStruct {
    return {
      path: this.path
    } as DriveStruct
  }
}
