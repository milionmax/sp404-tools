import { expect } from 'chai'

import { bytesToInt, intToHex } from '../src/utils'

describe('utils', () => {
  describe('bytesToInt', () => {

    it('should convert bytes to big-endian int', () => {
      let buff = Buffer.from([0, 1])
      expect(bytesToInt(buff)).to.equal(1)
      buff = Buffer.from([172, 68])
      expect(bytesToInt(buff)).to.equal(44100)
    })

    it('should convert bytes to little-endian int', () => {
      let buff = Buffer.from([1, 0])
      expect(bytesToInt(buff, false)).to.equal(1)
      buff = Buffer.from([68, 172])
      expect(bytesToInt(buff, false)).to.equal(44100)
    })
  })

  describe('intToHex', () => {

    it('should convert number to little-endian hex', () => {
      expect(intToHex(1, 1, true)).to.equal('01')
      expect(intToHex(44100, 4, true)).to.equal('44AC0000')
    })

    it('should pad little-endian with zeros properly', () => {
      expect(intToHex(1, 6, true)).to.equal('010000000000')
      expect(intToHex(44100, 6, true)).to.equal('44AC00000000')
    })
  })
})
