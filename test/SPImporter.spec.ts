import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import Drive from '../src/Drive'
import Sample from '../src/Sample'
import SampleStruct from '../src/interfaces/SampleStruct'
import SPImporter from '../src/SPImporter'

describe('SPImporter', () => {
  let drive!: Drive
  let samples: SampleStruct[] = []

  before(async () => {
    // Copy needed files into mock SP drive
    drive = new Drive(path.resolve(__dirname, 'data/SP-404SX'))
    await fs.promises.copyFile(path.resolve(__dirname, 'data/PAD_INFO.BIN'), drive.padInfoPath)

    const importer = new SPImporter(drive.toStruct())
    samples = await importer.importSamples()
  })

  after(async () => {
    fs.promises.unlink(drive.padInfoPath)
  })

  describe('importSamples()', () => {
    it('should import a full set of samples', () => {
      expect(samples.length).to.equal(120)
    })

    it('should import loaded samples', () => {
      let s1 = Sample.fromStruct(samples[0]) // A1
      expect(s1.isEmpty()).to.be.false
      let s2 = Sample.fromStruct(samples[15]) // B4
      expect(s2.isEmpty()).to.be.false
    })

    it('should "import" empty samples', () => {
      for (const struct of samples) {
        const sample = Sample.fromStruct(struct)
        if (sample.absPadIndex !== 0 // A1
          && sample.absPadIndex !== 15 // B4
        ) {
          expect(sample.isEmpty()).to.be.true
        }
      }
    })
  })
})
