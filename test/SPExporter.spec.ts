import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import Drive from '../src/Drive'
import Sample from '../src/Sample'
import SampleStruct from '../src/interfaces/SampleStruct'
import SPExporter, { SPExporterOp } from '../src/SPExporter'
import SPImporter from '../src/SPImporter'

describe('SPExporter', () => {
  let importer!: SPImporter
  let exporter!: SPExporter
  let drive!: Drive
  let samples: SampleStruct[] = []

  before(() => {
    const drivePath = path.resolve(__dirname, 'data/SP-404SX')
    drive = new Drive(drivePath)
    importer = new SPImporter(drive)
    exporter = new SPExporter(drive)
  })

  beforeEach(async () => {
    // copy PAD_INFO, A1 and B4 files to mock drive
    await fs.promises.copyFile(
      path.resolve(drive.path, '../PAD_INFO.BIN'),
      drive.padInfoPath
    )
    await fs.promises.copyFile(
      path.resolve(__dirname, 'data/A0000001.WAV'),
      drive.samplePathFromIndex('A', 1)
    )
    await fs.promises.copyFile(
      path.resolve(__dirname, 'data/B0000004.WAV'),
      drive.samplePathFromIndex('B', 4)
    )

    // import samples
    samples = await importer.importSamples()
  })

  afterEach(async () => {
    // Clear mock drive
    try {
      await fs.promises.unlink(drive.padInfoPath)
      await fs.promises.unlink(drive.samplePathFromIndex('A', 1))
      await fs.promises.unlink(drive.samplePathFromIndex('B', 4))
    } catch (err) {
      if (err.code !== 'ENOENT') throw err
    }
  })

  /*** ACTUAL TESTS START HERE ***/

  describe('exportSamples()', () => {
    it('should always "export" for all 120 pads', async () => {
      const populatedSamples = [samples[0]/*A1*/, samples[15]/*B4*/]
      const results = await exporter.exportSamples(populatedSamples)
      expect(results.length).to.equal(120)
    })

    it('should CLEAR deleted samples', async () => {
      const results = await exporter.exportSamples()
      expect(() => fs.accessSync(samples[0].sourcePath, fs.constants.F_OK)).to.throw()
      expect(() => fs.accessSync(samples[15].sourcePath, fs.constants.F_OK)).to.throw()
    })

    it('should WRITE copied samples', async () => {
      let populatedSamples = [samples[0]/*A1*/, samples[15]/*B4*/]
      const B1 = new Sample('B', 1, samples[0].sourcePath)
      populatedSamples.push(B1.toStruct())

      const results = await exporter.exportSamples(populatedSamples)
      const B1Exported = results[12]
      expect(B1Exported).to.deep.equal(Object.assign({}, B1.toStruct(), {
        sourcePath: B1Exported.sourcePath // sourcePath will be overwritten to newly exported file
      }))

      // Expect copied sample file to be there
      expect(() => fs.accessSync(B1Exported.sourcePath, fs.constants.F_OK)).to.not.throw()

      // Delete copied sample
      await fs.promises.unlink(B1Exported.sourcePath)
    })

    it('should WRITE before CLEARing', async () => {
      const A1Path = samples[0].sourcePath
      const B1 = new Sample('B', 1, A1Path)

      // Implicitly deleting A1 and B1 by not passing to exportSamples
      // A1 will be cleared, but that should be done AFTER B1 gets written,
      // otherwise B1's write op will fail with a ENOENT error
      const results = await exporter.exportSamples([B1.toStruct()])
      const A1Exported = results[0/*A1*/]
      const B1Exported = results[12/*B1*/]
      expect(A1Exported.sourcePath).to.equal('')
      expect(B1Exported).to.deep.equal(Object.assign({}, B1.toStruct(), {
        sourcePath: B1Exported.sourcePath // sourcePath will be overwritten to newly exported file
      }))
      
      // Expect A1 file to be deleted
      expect(() => fs.accessSync(A1Path, fs.constants.F_OK)).to.throw()

      // Expect copied sample file to be there
      expect(() => fs.accessSync(B1Exported.sourcePath, fs.constants.F_OK)).to.not.throw()

      // Delete copied sample
      await fs.promises.unlink(B1Exported.sourcePath)
    })

    it('should WRITE overwritten samples', async () => {
      const A1 = samples[0]
      let B4 = samples[15]

      // Overwrite B4 with a copy of A1
      B4.sourcePath = A1.sourcePath

      const results = await exporter.exportSamples([A1, B4])
      const B4Exported = results[15]/*B4*/
      expect(results[0]/*A1*/).to.deep.equal(A1)
      expect(B4Exported).to.deep.equal(Object.assign({}, B4, {
        sourcePath: B4Exported.sourcePath // sourcePath will be overwritten to newly exported file
      }))

      const A1Buff = await fs.promises.readFile(drive.samplePathFromIndex('A', 1))
      const B4Buff = await fs.promises.readFile(drive.samplePathFromIndex('B', 4))
      // Compare sample data (excluding file headers)
      expect(A1Buff.slice(512).toString('hex')).to.equal(B4Buff.slice(512).toString('hex'))
    })

    it('should not alter sample pad info fields', async () => {
      let A1 = Sample.fromStruct(samples[0])
      const origPadInfo = await A1.getPadInfo()
      const results = await exporter.exportSamples(samples)
      A1 = Sample.fromStruct(results[0]) // reload
      const padInfo = await A1.getPadInfo()
      expect(origPadInfo).to.deep.equal(padInfo)
    })
  })

  describe('exportPadInfoBin()', () => {
    it('should not change on simple import->export', async () => {
      const initPadInfoBuff = await fs.promises.readFile(drive.padInfoPath)
      const result = await exporter.exportPadInfoBin(samples)
      const newPadInfoBuff = await fs.promises.readFile(drive.padInfoPath)
      expect(initPadInfoBuff.toString('hex')).to.equal(newPadInfoBuff.toString('hex'))
    })

    it('should write pad info for new samples', async () => {
      const A1 = Sample.fromStruct(samples[0])
      const B1 = new Sample('B', 1, A1.sourcePath)
      const B4 = Sample.fromStruct(samples[15])

      const B1PadInfo = await B1.getPadInfo()
      expect(B1PadInfo.fields.origSampleEnd).to.be.above(512) // sanity check: not empty

      const B1Offset = B1.absPadIndex*32

      const oldPadInfoBuff = await fs.promises.readFile(drive.padInfoPath)
      const oldB1LineHex = oldPadInfoBuff.slice(B1Offset, B1Offset+32).toString('hex')

      const result = await exporter.exportPadInfoBin([A1, B1, B4])
      const newPadInfoBuff = await fs.promises.readFile(drive.padInfoPath)

      const B1PadInfoHex = B1PadInfo.toBuffer().toString('hex')
      const B1LineHex = newPadInfoBuff.slice(B1Offset, B1Offset+32).toString('hex')

      expect(B1PadInfoHex).to.not.equal(oldB1LineHex)
      expect(B1PadInfoHex).to.equal(B1LineHex)
    })
  })
})
