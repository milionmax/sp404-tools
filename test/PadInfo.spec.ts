import { expect } from 'chai'

import PadInfo from '../src/PadInfo'
import { DEFAULTS } from '../src/PadInfoFields'
import PadInfoStruct from '../src/interfaces/PadInfoStruct'

describe('PadInfo', () => {
  const EMPTY_PAD_BUFF = Buffer.from([
    0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00,
    0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00,
    0x7f, 0x00, 0x00, 0x01, 0x00, 0x01, 0x02, 0x00,
    0x00, 0x00, 0x04, 0xb0, 0x00, 0x00, 0x04, 0xb0
  ])

  const LOADED_PAD_BUFF = Buffer.from([
    0x00, 0x00, 0x02, 0x00, 0x03, 0x00, 0x80, 0x00,
    0x00, 0x00, 0x02, 0x00, 0x03, 0x00, 0x80, 0x00,
    0x73, 0x01, 0x01, 0x00, 0x01, 0x01, 0x01, 0x00,
    0x00, 0x00, 0x04, 0x36, 0x00, 0x00, 0x04, 0x36
  ])

  describe('toBuffer()', () => {
    it('should output standard empty PAD_INFO line for new PadInfo', () => {
      const padInfo = new PadInfo()
      const buff = padInfo.toBuffer()
      expect(buff).to.deep.equal(EMPTY_PAD_BUFF)
    })
  })

  describe('fromBuffer()', () => {
    it('should read in the correct values', () => {
      const padInfo = PadInfo.fromBuffer(LOADED_PAD_BUFF)
      expect(padInfo.fields.origSampleStart).to.equal(512)
      expect(padInfo.fields.origSampleEnd).to.equal(50364416)
      expect(padInfo.fields.userSampleStart).to.equal(512)
      expect(padInfo.fields.userSampleEnd).to.equal(50364416)
      expect(padInfo.fields.volume).to.equal(115)
      expect(padInfo.fields.lofi).to.equal(true)
      expect(padInfo.fields.loop).to.equal(true)
      expect(padInfo.fields.gate).to.equal(false)
      expect(padInfo.fields.reverse).to.equal(true)
      expect(padInfo.fields.format).to.equal(1)
      expect(padInfo.fields.channels).to.equal(1)
      expect(padInfo.fields.tempoMode).to.equal(0)
      expect(padInfo.fields.origTempo).to.equal(1078)
      expect(padInfo.fields.userTempo).to.equal(1078)
    })
  })

  describe('indexFromLineNo()', () => {
    it('should return A/1 for 0', () => {
      const [ bank, pad ] = PadInfo.indexFromLineNo(0)
      expect(bank).to.equal('A')
      expect(pad).to.equal(1)
    })

    it('should return J/12 for 119', () => {
      const [ bank, pad ] = PadInfo.indexFromLineNo(119)
      expect(bank).to.equal('J')
      expect(pad).to.equal(12)
    })
  })

  describe('isEmptyPad()', () => {
    it('should return true for default pad', () => {
      const padInfo = new PadInfo()
      expect(padInfo.isEmptyPad()).to.be.true
    }) 

    it('should return false for loaded pad', () => {
      const padInfo = PadInfo.fromBuffer(LOADED_PAD_BUFF)
      expect(padInfo.isEmptyPad()).to.be.false
    }) 
  })

  describe('fromStruct()', () => {
    it('should preserve fields', () => {
      const struct: PadInfoStruct = {
        fields: Object.assign({}, DEFAULTS, {
          loop: true, // change some things
          volume: 100 
        })
      }

      const padInfo: PadInfo = PadInfo.fromStruct(struct)
      expect(padInfo.fields).to.deep.equal(struct.fields)
    })
  })

  describe('toStruct()', () => {
    it('should preserve fields', () => {
      const padInfo: PadInfo = new PadInfo()
      const struct: PadInfoStruct = padInfo.toStruct()
      expect(struct.fields).to.equal(padInfo.fields)
    })
  })
})
